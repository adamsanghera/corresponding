tmp_file="$(mktemp)"
url="http://localhost:4000"

# good `set`
curl --silent -X POST "$url/set" --header "Content-Type: application/json" --data '{"username":"xyz","password":"xyz"}' >> $tmp_file
# bad `get` (bad method, `POST`)
curl --silent -X POST "$url/get?key=username" >> $tmp_file
# good `get`
curl --silent -X GET  "$url/get?key=password" >> $tmp_file
# good `delete`
curl --silent -X POST "$url/delete?key=username" >> $tmp_file
# bad `delete` (key does not exist)
curl --silent -X POST "$url/delete?key=username" >> $tmp_file

diff $tmp_file ./test/expected_out.txt

if [ $? -eq 1 ]; then
    echo "tests failed"
else
    echo "tests passed"
fi
