from flask import Flask, abort, jsonify, request


app = Flask(__name__)
app.debug = True

# NOTE: we don't have any lock gating access to this data structure.
# Concurrent `delete` requests have the potential to go astray, since we make two
# atomic queries to `kv` (the `in` check, and `pop`). It's possible that two concurrent
# requests pass the `in` check, only for one to raise `KeyError` while `pop`ping the key.
kv = dict()


def _get_key_from_request():
    key = request.args.get("key", "")
    if not key:
        return abort(404)

    return key


@app.route("/set", methods=["POST"])
def set():
    kv.update(request.json)
    return jsonify(request.json)


@app.route("/get", methods=["GET"])
def get():
    key = _get_key_from_request()

    # NOTE: we don't check that `key` exists, will raise `KeyError` if it does not exist
    return jsonify(dict(key=key, value=kv[key]))


@app.route("/delete", methods=["POST"])
def delete():
    key = _get_key_from_request()

    if key not in kv:
        return jsonify(dict(error_message="Key '{}' does not exist".format(key)))

    return jsonify(dict(key=key, value=kv.pop(key)))
